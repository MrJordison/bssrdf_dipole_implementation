


************** VS *****************


************** FS *****************

#define PI radians(180)

vec3[] wi_s;
vec3[] xi_s;

float ind_n;
vec3 x0;
vec3 w0;
vec3 n;
float albedo;

void main(){

  vec3 color = vec3(0);
  
  for(int i = 0; i < wi_s.length; ++i){
  
    color += bssrdf(xi_s[i], wi_s[i]);
    
  }
  
  fragColor = vec4(color,1);
}

vec3 bssrdf(vec3 xi, vec3 wi){
  
  vec3 diffuse = diffuse(xi, wi);
  
  vec3 scattering_term = scattering_term (xi, wi);
  
  return diffuse + scattering_term;
}

vec3 diffuse(vec3 xi, vec3 wi){
  
  ft_wi = ft(ind_n, wi);
  
  ft_w0 = ft(ind_n, w0);
  
  rd = rd(length(xi - x0));
  
  return (1 / PI) * ft_wi * rd * ft_w0;
}

float ft(ind_milieu, vec3 dir){

}

float rd(float dist){

  float scale = albedo_prime / (4 * PI);
  
  float term1 = sigma_tr * d_r + 1;
  float term2 = z_v * (sigma_tr * dv + 1);
  
  float fraction1 = exp ((-sigma_tr) * d_r) / (sigma_t_prime * pow(d_r,3));
  
  float fraction2 = exp((-sigma_tr) * d_v) / (sigma_t_prime * pow(d_v,3));
  
  return scale * (term1 * fraction1 + term2 * fraction2);
  
}